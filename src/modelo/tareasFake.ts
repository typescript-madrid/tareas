import { Tarea } from './tarea.interface';

export let tareasMockUp: Tarea[] = [
  {
    id: 1,
    descripcion: 'Estudiar JavaScript',
    realizada: false
  },
  {
    id: 2,
    descripcion: 'Estudiar TypeScript',
    realizada: true
  }
];
