export interface Tarea {
  id: number;
  descripcion: string;
  realizada: boolean;
}
