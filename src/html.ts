import { Tarea } from './modelo/tarea.interface';
import { AppTareas } from './appTareas';

export class TareasHTML {
  private $tareaBase: JQuery;
  private $listaTareas: JQuery;
  private $botonBorrarCompletadas: JQuery;
  private $formAnyadir: JQuery;
  private $inputDescripcion: JQuery;
  private tareas: Tarea[];

  constructor(private appTareas: AppTareas) {
    this.tareas = appTareas.tareas;
  }

  public inicializaHTML(): void {
    this.$tareaBase = $('.tarea');
    this.$listaTareas = $('.lista-todos');
    this.$botonBorrarCompletadas = $('#borrar input');
    this.$formAnyadir = $('#anyadir-todo');
    this.$inputDescripcion = $('.descripcion');
  }

  private vaciaLista(): void {
    this.$listaTareas.find('.tarea').remove();
  }

  public rellenaLista(): void {
    this.vaciaLista();
    for (let tarea of this.tareas) {
      let $nuevaTarea = this.$tareaBase.clone();
      $nuevaTarea.removeClass('base');
      this.rellenaTarea(tarea, $nuevaTarea);
      this.escuchaClickTareas($nuevaTarea);
      this.$listaTareas.append($nuevaTarea);
    }
  }

  private rellenaTarea(tarea: Tarea, $tarea: JQuery): void {
    $tarea.attr('id', `tarea-${tarea.id}`);
    let $texto = $tarea.find('.texto');
    $texto.text(tarea.descripcion);
    let $checkFinalizada = $tarea.find('.check-todo');
    $checkFinalizada.prop('checked', tarea.realizada);
    if (tarea.realizada) {
      $tarea.addClass('terminado');
    } else {
      $tarea.removeClass('terminado');
    }
  }

  private borrarCompletadas() {
    this.appTareas.borrarTareasCompletadas();
    this.rellenaLista();
  }

  public escuchaBotonBorrar(): void {
    this.$botonBorrarCompletadas.on('click', e => {
      e.preventDefault();
      this.borrarCompletadas();
    });
  }

  public escuchaBotonAnyadir(): void {
    this.$formAnyadir.on('submit', e => {
      e.preventDefault();
      this.anyadirTarea();
    });
  }

  public escuchaClickTareas($tarea: JQuery): void {
    $tarea.find('.check-todo').on('change', () => {
      let idTarea = Number($tarea.attr('id').split('-').pop());
      let realizada = this.appTareas.modificarEstadoTarea(idTarea);
      if (realizada) {
        $tarea.addClass('terminado');
      } else {
        $tarea.removeClass('terminado');
      }
    });
  }

  public anyadirTarea(): void {
    let descripcion = <string>this.$inputDescripcion.val();
    this.appTareas.creaTarea(descripcion);
    this.$inputDescripcion.val('');
    this.$inputDescripcion.focus();
    this.rellenaLista();
  }
}
