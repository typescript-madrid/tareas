import { Tarea } from './modelo/tarea.interface';

export class AppTareas {

  public tareas: Array<Tarea> = [];

  constructor() {
    this.cargarTareasAPI();
  }

  private cargarTareasAPI(): void {

    $.getJSON('http://localhost:3000/tareas', (datos) => {
      for (let dato of datos) {
        this.tareas.push(dato);
      }
      $(document).trigger('datos-cargados');
    });

  }

  private generaId(): number {
    // let idMaxima = this.tareas.reduce((acum, tarea) => Math.max(acum, tarea.id), 0);
    let idMaxima = Math.max(...this.tareas.map(tarea => tarea.id));
    return idMaxima + 1;
  }

  public creaTarea(descripcion: string) {
    let nuevaTarea: Tarea = {
      id: this.generaId(),
      descripcion: descripcion,
      realizada: false
    };
    this.tareas.push(nuevaTarea);
  }

  public modificarEstadoTarea(id: number): boolean {
    let tarea = this.tareas.find(t => t.id === id);
    tarea.realizada = !tarea.realizada;
    return tarea.realizada;
  }

  public borrarTareasCompletadas(): void {
    this.tareas.forEach((tarea, i, tareas) => {
      if (tarea.realizada) {
        tareas.splice(i, 1);
      }
    });
  }
}
