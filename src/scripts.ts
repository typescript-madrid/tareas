import { AppTareas } from './appTareas';
import { TareasHTML } from './html';

let appTareas = new AppTareas();
let tareasHTML = new TareasHTML(appTareas);

$(document).on('datos-cargados', () => {
  tareasHTML.rellenaLista();
});

tareasHTML.inicializaHTML();
tareasHTML.escuchaBotonBorrar();
tareasHTML.escuchaBotonAnyadir();
